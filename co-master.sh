#!/bin/bash

current=`pwd`
bundle=`ls ./bundle`
for script in $bundle
do
    cd $current/bundle/$script
    git co master
    echo "checkout $script master"
    cd $current
done

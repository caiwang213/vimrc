""""""""""""""""""""""""""""""
" => vundle
" [snipMate.vim] (https://github.com/garbas/vim-snipmate): snipMate.vim aims to be a concise vim script that implements some of TextMate's snippets features in Vim
" [bufexplorer.zip] (https://github.com/vim-scripts/bufexplorer.zip): Buffer Explorer / Browser. This plugin can be opened with `<leader+o>`
" [NERD Tree] (https://github.com/scrooloose/nerdtree): A tree explorer plugin for vim
" [ack.vim] (https://github.com/mileszs/ack.vim): Vim plugin for the Perl module / CLI script 'ack'
" [ag.vim] (https://github.com/rking/ag.vim): A much faster Ack
" [ctrlp.vim] (https://github.com/ctrlpvim/ctrlp.vim): Fuzzy file, buffer, mru and tag finder. In my config it's mapped to `<Ctrl+F>`, because `<Ctrl+P>` is used by YankRing
" [mru.vim] (https://github.com/vim-scripts/mru.vim): Plugin to manage Most Recently Used (MRU) files. Includes my own fork which adds syntax highlighting to MRU. This plugin can be opened with `<leader+f>`
" [open_file_under_cursor.vim] (https://github.com/amix/open_file_under_cursor.vim): Open file under cursor when pressing `gf`
" [vim-indent-object] (https://github.com/michaeljsmith/vim-indent-object): Defines a new text object representing lines of code at the same indent level. Useful for python/vim scripts
" [vim-multiple-cursors] (https://github.com/terryma/vim-multiple-cursors): Sublime Text style multiple selections for Vim, CTRL+N is remapped to CTRL+S (due to YankRing)
" [vim-expand-region] (https://github.com/terryma/vim-expand-region): Allows you to visually select increasingly larger regions of text using the same key combination.
" [vim-fugitive] (https://github.com/tpope/vim-fugitive): A Git wrapper so awesome, it should be illegal
" [goyo.vim] (https://github.com/junegunn/goyo.vim) and [vim-zenroom2](https://github.com/amix/vim-zenroom2): Remove all clutter and focus only on the essential. Similar to iA Writer or Write Room [Read more here](http://amix.dk/blog/post/19744)
" [vim-commentary] (https://github.com/tpope/vim-commentary): Comment stuff out.  Use `gcc` to comment out a line (takes a count), `gc` to comment out the target of a motion. `gcu` uncomments a set of jacent commented lines.
" [syntastic] (https://github.com/scrooloose/syntastic): Syntax checking hacks for vim
" [vim-yankstack] (https://github.com/maxbrunsfeld/vim-yankstack): Maintains a history of previous yanks, changes and deletes
" [lightline.vim] (https://github.com/itchyny/lightline.vim): A light and configurable statusline/tabline for Vim
" [ultisnips] like with snipMate
"
" 常用命令
" :PluginList               - 查看已经安装的插件
" :PluginInstall            - 安装插件
" :PluginUpdate  [plugin]   - 更新插件
" :PluginSearch             - 搜索插件，例如 :PluginSearch xml就能搜到xml相关的插件
" :PluginClean              - 删除插件，把安装插件对应行删除，然后执行这个命令即可
"
" :h Plugin for more information
""""""""""""""""""""""""""""""
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

" @Plugin [misc]
Plugin 'skywind3000/asyncrun.vim'
Plugin 'caiwang213/misc.vim'

" terminal in vim
" Plugin 'pthrasher/conqueterm-vim'

" @Plugin [vim view]
Plugin 'scrooloose/nerdtree'
Plugin 'majutsushi/tagbar'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
" Plugin 'itchyny/lightline.vim'

" @Plugin filetype
Plugin 'elzr/vim-json'

" @Plugin [file search]
Plugin 'rking/ag.vim'

" @Plugin [buffer manage]
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'vim-scripts/mru.vim'
Plugin 'vim-scripts/bufexplorer.zip'
Plugin 'danro/rename.vim'
Plugin 'Yggdroot/LeaderF'
" Plugin 'fholgado/minibufexpl.vim'

" @Plugin [code jump]
Plugin 'vim-scripts/a.vim'
Plugin 'vim-scripts/taglist.vim'
Plugin 'https://gitee.com/caiwang213/cscope-maps.vim'
Plugin 'chxuan/cpp-mode'
"Plugin 'L9'

" @Plugin [code complete]
Plugin 'Valloric/YouCompleteMe.git'
Plugin 'rdnetto/YCM-Generator'
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'             " the snippet source for ultisnips and snipMate
" Plugin 'snipMate'
Plugin 'tpope/vim-surround'
"Plugin 'Townk/vim-autoclose'
Plugin 'Shougo/echodoc.vim'
Plugin 'jiangmiao/auto-pairs'

" @Plugin [code systastic]
" Plugin 'scrooloose/syntastic'
" Plugin 'w0rp/ale'

" @Plugin [code format]
Plugin 'Chiel92/vim-autoformat'
Plugin 'junegunn/vim-easy-align'

" @Plugin [character search/move]
" Plugin 'haya14busa/incsearch.vim'            " do not work!
Plugin 'justinmk/vim-sneak'
" Plugin 'rhysd/clever-f.vim'
" Plugin 'junegunn/vim-slash'

" " @Plugin [code block select]
Plugin 'terryma/vim-multiple-cursors'
Plugin 'terryma/vim-expand-region'
" Plugin 'kana/vim-operator-user'            " do not work!
" Plugin 'haya14busa/vim-operator-flashy'    " do not work!

" " @Plugin [code comment]
Plugin 'tpope/vim-commentary'

" @Pugin [git/svn]
Plugin 'airblade/vim-gitgutter'
" Plugin 'mhinz/vim-signify'
Plugin 'tpope/vim-fugitive'
" Plugin 'tpope/vim-git'
Plugin 'gregsexton/gitv'

" @Pugin [repeat]
Plugin 'tpope/vim-repeat'

" @Pugin [debug]
Plugin 'vim-scripts/vimgdb'

" @Pugin [colorscheme]
" Plugin 'DemonCloud/J'

"Plugin 'L9'

call vundle#end()


""""""""""""""""""""""""""""""
" => set common attribute
""""""""""""""""""""""""""""""
set fileencodings=utf-8,cp936,ucs-bom,gb2312,big5,latin1,gbk,euc-kr,ios8859-1,euc-jp,gb18030,ios8859,chinese
set formatoptions=crqlmM
" set formatoptions+=mM
" set formatoptions-=o
" set formatoptions=crqlmM



""""""""""""""""""""""""""""""
" => set gf search path
""""""""""""""""""""""""""""""
let incs=system("find . -type d|grep include")
"set path+=%{incs}%
set path+=core/common/,core/include/
set tags+=.tags

""""""""""""""""""""""""""""""
" => maps
""""""""""""""""""""""""""""""
inoremap <f1>  :sp /home/caiwang213/.vim/bundle/misc.vim/doc/misc.txt<cr>
nnoremap <f1>  :sp /home/caiwang213/.vim/bundle/misc.vim/doc/misc.txt<cr>
inoremap 1<f1> <esc>:sp /home/caiwang213/workspace/git/config-backup/vihelp.vim<cr>
nnoremap 1<f1> :sp /home/caiwang213/workspace/git/config-backup/vihelp.vim<cr>
inoremap <f4>  <esc>:!/home/caiwang213/workspace/git/config-backup/cscope.sh<cr>:cs reset<cr>
nnoremap <f4>  :!/home/caiwang213/workspace/git/config-backup/cscope.sh<cr>:cs reset<cr>
nnoremap <f5>  :cd build<cr>:make<cr>:cd ..<cr>
" nnoremap <f5>  :cd ../build-media/linux/x64<cr>:make<cr>:cd -<cr>
nnoremap K     :call ManK()<cr>
nnoremap 2K    :call Man2K()<cr>
nnoremap 3K    :call Man3K()<cr>
nnoremap 4K    :call Man4K()<cr>
nnoremap 5K    :call Man5K()<cr>
nnoremap 6K    :call Man6K()<cr>
nnoremap 7K    :call Man6K()<cr>

nnoremap +     ggVG=<c-o><c-o>
nmap     //    gcc
vmap     //    gc
inoremap {}    {<cr>}<Esc>O
inoremap 'p printf("\n");<Esc>hhhhi


""""""""""""""""""""""""""""""
" =>  same as shell commond
""""""""""""""""""""""""""""""
inoremap <c-b> <left>
inoremap <c-f> <right>
inoremap <c-a> <home>
inoremap <c-e> <end>
" inoremap <c-h>
inoremap <c-d> <del>
" inoremap <c-w>
" inoremap <c-u>
inoremap <c-k> <esc>lC
inoremap <c-l> <esc>lce


""""""""""""""""""""""""""""""
" => swap on both sides of '=' 
""""""""""""""""""""""""""""""
nnoremap <leader>s   :%s/\(^\s*\)\(.*[^+]\) = \(.*\);$/\1\3 = \2;/g
vnoremap <leader>s   :s/\(^\s*\)\(.*[^+]\) = \(.*\);$/\1\3 = \2;/g
nnoremap <leader>sc  :%s/\(^\s*\)\(.*[^+]\) = \(.*\);$/\1\3 = \2;/gc
vnoremap <leader>sc  :s/\(^\s*\)\(.*[^+]\) = \(.*\);$/\1\3 = \2;/gc

""""""""""""""""""""""""""""""
" => unmap
" for a.vim cause space work snowly in insert mode
""""""""""""""""""""""""""""""
" iunmap <leader>ih
" iunmap <leader>is
" iunmap <leader>ihn
" nunmap  <c-n>
" nunmap  <c-m>


""""""""""""""""""""""""""""""
" => cscope
""""""""""""""""""""""""""""""
if has("cscope") && filereadable("/usr/bin/cscope")
    set csprg=/usr/bin/cscope
    set csto=0
    set cscopetag
    set nocsverb
    " add any database in current directory
    if filereadable(".cscope.out")
        cs add $PWD/.cscope.out
        " else add database pointed to by environment
    elseif $CSCOPE_DB != ""
        cs add $CSCOPE_DB
    endif
    set csverb
    " show msg when any other cscope db added
    set cscopeverbose
    " set cscopequickfix=s-,c-,d-,i-,t-,e-
    set cscopequickfix=s-,c-,d-,i-,t-,e-,g-
    " nmap <C-[>s :cs find s <C-R>=expand("<cword>")<CR><CR>:copen<CR>
    " nmap <C-[>g :cs find g <C-R>=expand("<cword>")<CR><CR>:copen<CR>
    " nmap <C-[>c :cs find c <C-R>=expand("<cword>")<CR><CR>:copen<CR>
    " nmap <C-[>t :cs find t <C-R>=expand("<cword>")<CR><CR>:copen<CR>
    " nmap <C-[>e :cs find e <C-R>=expand("<cword>")<CR><CR>:copen<CR>
    " nmap <C-[>f :cs find f <C-R>=expand("<cfile>")<CR><CR>:copen<CR>
    " nmap <C-[>i :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR>:copen<CR>
    " nmap <C-[>d :cs find d <C-R>=expand("<cword>")<CR><CR>:copen<CR>
    nmap <C-[>s :cs find s <C-R>=expand("<cword>")<CR><CR>
    nmap <C-[>g :cs find g <C-R>=expand("<cword>")<CR><CR>
    nmap <C-[>c :cs find c <C-R>=expand("<cword>")<CR><CR>
    nmap <C-[>t :cs find t <C-R>=expand("<cword>")<CR><CR>
    nmap <C-[>e :cs find e <C-R>=expand("<cword>")<CR><CR>
    nmap <C-[>f :cs find f <C-R>=expand("<cfile>")<CR><CR>
    nmap <C-[>i :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
    nmap <C-[>d :cs find d <C-R>=expand("<cword>")<CR><CR>
endif

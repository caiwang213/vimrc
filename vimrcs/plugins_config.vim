
""""""""""""""""""""""""""""""
" => Tagbar plugin
""""""""""""""""""""""""""""""
nmap <F8> :TagbarToggle<CR>
let g:tagbar_left = 1
" let g:tagbar_vertical = 30
let g:tagbar_autoclose = 0


""""""""""""""""""""""""""""""
" => bufExplorer plugin
""""""""""""""""""""""""""""""
let g:bufExplorerSplitBelow       = 1
let g:bufExplorerDefaultHelp      = 1
let g:bufExplorerShowRelativePath = 1
let g:bufExplorerFindActive       = 1
let g:bufExplorerSortBy           = 'name'
let g:bufExplorerShowNoName       = 0 
map <leader>o :BufExplorer<cr>
" map <leader>o :BufExplorerHorizontalSplit<cr>


""""""""""""""""""""""""""""""
" => CTRL-P
""""""""""""""""""""""""""""""
let g:ctrlp_working_path_mode = 0

" let g:ctrlp_map = '<c-f>'
" map <leader>j :CtrlP<cr>
" map <c-b> :CtrlPBuffer<cr>

let g:ctrlp_max_height    = 20
let g:ctrlp_custom_ignore = 'node_modules\|^\.DS_Store\|^\.git\|^\.coffee'


""""""""""""""""""""""""""""""
" => MRU plugin
""""""""""""""""""""""""""""""
let MRU_Auto_Close = 1
let MRU_Max_Entries = 100
map <leader>f :MRU<CR>


""""""""""""""""""""""""""""""
" => Nerd Tree
""""""""""""""""""""""""""""""
let g:NERDTreeWinPos   = "right"
let NERDTreeShowHidden = 0
let NERDTreeIgnore     = ['\.o', '\~$', '\.sln$', '\.vcxproj*', '\.lib', 'Debug*', 'Release*' , '.aps', 'Win32']
let g:NERDTreeWinSize  = 35
map <leader>nn :NERDTreeToggle<cr>
map <leader>nb :NERDTreeFromBookmark
map <leader>nf :NERDTreeFind<cr>


""""""""""""""""""""""""""""""
" => vim-multiple-cursors,map
""""""""""""""""""""""""""""""
let g:multi_cursor_use_default_mapping=0
let g:multi_cursor_start_key = "g<c-j>"
let g:multi_cursor_next_key  = "<c-j>"
let g:multi_cursor_prev_key  = "<c-k>"
let g:multi_cursor_skip_key  = "<c-x>"
let g:multi_cursor_quit_key  = '<Esc>'


""""""""""""""""""""""""""""""
" => YouCompleteMe
""""""""""""""""""""""""""""""
let g:ycm_server_python_interpreter='/opt/python-3.6.6/bin/python3'
let g:ycm_global_ycm_extra_conf='~/.vim/.ycm_extra_conf.py'

" .ycm_extra_conf.py在当前文件夹下不需要提示
let g:ycm_confirm_extra_conf = 0

let g:ycm_key_invoke_completion = '<S-Space>'

let g:ycm_error_symbol = '>>'
let g:ycm_warning_symbol = '>*'
let g:ycm_complete_in_comments = 1

set completeopt=preview
let g:ycm_add_preview_to_completeopt = 1
let g:ycm_autoclose_preview_window_after_insertion = 1
"set completeopt=menuone
let g:ycm_disable_for_files_larger_than_kb = 10*1024

map <leader>gd :YcmCompleter GoToDeclaration<cr>
map <leader>gg :YcmCompleter GoToDefinition<cr>
map <leader>gi :YcmCompleter GoToInclude<cr>
map <leader>gf :YcmCompleter FixIt<cr>
map <leader>ge :YcmDiags<cr>
map <leader>gr :YcmRestartServer<cr>

""""""""""""""""""""""""""""""
" => syntastic
""""""""""""""""""""""""""""""
" set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
" set statusline+=%*

" let g:syntastic_always_populate_loc_list = 1
" let g:syntastic_auto_loc_list = 1
" let g:syntastic_check_on_wq = 0
" let g:syntastic_check_on_open = 1
" let g:syntastic_auto_jump = 1
" let g:syntastic_cpp_include_dirs = ['core/common', 'core/include']

" let g:syntastic_enable_highlighting = 1 " 错误单词高亮
" let g:syntastic_aggregate_errors = 1 "第一时间检查发现任何错误
" let g:syntastic_enable_signs = 1
" let g:syntastic_stl_format = "[%E{Err: %fe #%e}%B{, }%W{Warn: %fw #%w}]"
" let g:syntastic_debug_file = "~/syntastic.log"
" let g:syntastic_cpp_checkers = ['gcc', 'g++']
" let g:syntastic_enable_balloons = 1

" let g:syntastic_cpp_compiler = 'g++'
" let g:syntastic_cpp_compiler_options = '-std=c++11 -Wall -Wextra'

" let g:syntastic_cpp_check_header = 1
" let g:syntastic_cpp_auto_refresh_includes = 1


""""""""""""""""""""""""""""""
" => snipMate (beside <TAB> support <CTRL-j>)
""""""""""""""""""""""""""""""
" inoremap <c-j> <c-r>=TriggerSnippet()<cr>
" snoremap <c-j> <esc>i<right><c-r>=TriggerSnippet()<cr>


""""""""""""""""""""""""""""""
" => ultisnips, Conflict with snipMate
""""""""""""""""""""""""""""""
let g:UltiSnipsExpandTrigger="<c-l>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"


""""""""""""""""""""""""""""""
" => a.vim, note: use iunmap that
" will cause space work snowly in insert mode
""""""""""""""""""""""""""""""
map <leader>a :A<cr>


""""""""""""""""""""""""""""""
" => airline
""""""""""""""""""""""""""""""
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ''
let g:airline#extensions#tabline#left_alt_sep = ''
" let g:airline#extensions#tabline#buffer_nr_show = 1
" let g:airline_theme='base16'
" let g:airline_theme='hybrid'
" let g:airline_theme='term'
let g:airline_theme='tomorrow'

" unicode symbols
let g:airline_left_sep = ' '
let g:airline_right_sep = ' '
" let g:airline_left_sep = '»'
" let g:airline_left_sep = '▶'
" let g:airline_right_sep = '«'
" let g:airline_right_sep = '◀'



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => lightline
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:lightline = {
            \ 'colorscheme': 'wombat',
            \ 'active': {
            \   'left': [ ['mode', 'paste'],
            \             ['fugitive', 'readonly', 'filename', 'modified'] ],
            \   'right': [ [ 'lineinfo' ], ['percent'] ]
            \ },
            \ 'component': {
            \   'readonly': '%{&filetype=="help"?"":&readonly?"�":""}',
            \   'modified': '%{&filetype=="help"?"":&modified?"+":&modifiable?"":"-"}',
            \   'fugitive': '%{exists("*fugitive#head")?fugitive#head():""}'
            \ },
            \ 'component_visible_condition': {
            \   'readonly': '(&filetype!="help"&& &readonly)',
            \   'modified': '(&filetype!="help"&&(&modified||!&modifiable))',
            \   'fugitive': '(exists("*fugitive#head") && ""!=fugitive#head())'
            \ },
            \ 'separator': { 'left': '|', 'right': ' ' },
            \ 'subseparator': { 'left': ' ', 'right': ' ' }
            \ }

" 关闭lightline tabline功能
let g:lightline.enable = {
            \ 'statusline': 1,
            \ 'tabline': 0
            \ }

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => my neat stayle tabline
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! NeatTabLine()
    let s = ''
    for i in range(tabpagenr('$'))
        " select the highlighting
        if i + 1 == tabpagenr()
            let s .= '%#TabLineSel#'
        else
            let s .= '%#TabLine#'
        endif
        " set the tab page number (for mouse clicks)
        let s .= '%' . (i + 1) . 'T'
        " the label is made by MyTabLabel()
        let s .= ' %{NeatTabLabel(' . (i + 1) . ')} '
    endfor
    " after the last tab fill with TabLineFill and reset tab page nr
    let s .= '%#TabLineFill#%T'
    " right-align the label to close the current tab page
    if tabpagenr('$') > 1
        let s .= '%=%#TabLine#%999XX'
    endif
    return s
endfunc

" get a single tab name
function! NeatBuffer(bufnr, fullname)
    let l:name = bufname(a:bufnr)
    if getbufvar(a:bufnr, '&modifiable')
        if l:name == ''
            return '[No Name]'
        else
            if a:fullname
                return fnamemodify(l:name, ':p')
            else
                return fnamemodify(l:name, ':t')
            endif
        endif
    else
        let l:buftype = getbufvar(a:bufnr, '&buftype')
        if l:buftype == 'quickfix'
            return '[Quickfix]'
        elseif l:name != ''
            if a:fullname
                return '-'.fnamemodify(l:name, ':p')
            else
                return '-'.fnamemodify(l:name, ':t')
            endif
        else
        endif
        return '[No Name]'
    endif
endfunc

" get a single tab label
function! NeatTabLabel(n)
    let l:buflist = tabpagebuflist(a:n)
    let l:winnr = tabpagewinnr(a:n)
    let l:bufnr = l:buflist[l:winnr - 1]
    return NeatBuffer(l:bufnr, 0)
endfunc

" get a single tab label in gui
function! NeatGuiTabLabel()
    let l:num = v:lnum
    let l:buflist = tabpagebuflist(l:num)
    let l:winnr = tabpagewinnr(l:num)
    let l:bufnr = l:buflist[l:winnr - 1]
    return NeatBuffer(l:bufnr, 0)
endfunc

" setup new tabline, just like %M%t in macvim
" set tabline=%!NeatTabLine()
" set guitablabel=%{NeatGuiTabLabel()}

noremap <silent><tab>n :tabn<cr>
noremap <silent><tab>p :tabp<cr>
noremap <silent><leader>t :tabnew<cr>
noremap <silent><leader>1 :tabn 1<cr>
noremap <silent><leader>2 :tabn 2<cr>
noremap <silent><leader>3 :tabn 3<cr>
noremap <silent><leader>4 :tabn 4<cr>
noremap <silent><leader>5 :tabn 5<cr>
noremap <silent><leader>6 :tabn 6<cr>
noremap <silent><leader>7 :tabn 7<cr>
noremap <silent><leader>8 :tabn 8<cr>
noremap <silent><leader>9 :tabn 9<cr>
noremap <silent><leader>0 :tabn 10<cr>

""""""""""""""""""""""""""""""
" => vim-fugitive
""""""""""""""""""""""""""""""
" set signcolumn=yes
set updatetime=200
let g:gitgutter_max_signs = 1000
nmap <leader>hh :GitGutterLineHighlightsToggle<cr>

""""""""""""""""""""""""""""""
" => vim-easy-align
""""""""""""""""""""""""""""""
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

" General alignment around whitespaces
vmap _  :EasyAlig*\<cr>

" Operators containing equals sign (=, ==, !=, +=, &&=, ...)
vmap +  :EasyAlign*=<cr>

" Multi-line method arguments
vmap ,  :EasyAlign*,<cr>

" LaTeX tables (matches & and \\)
vmap &  :EasyAlign*&<cr>

vmap .  :EasyAlign*.<cr>


""""""""""""""""""""""""""""""
" => vim-autoformat
""""""""""""""""""""""""""""""
noremap <F3> :Autoformat<CR>
" au BufWrite * :Autoformat
" let g:autoformat_autoindent = 0
" let g:autoformat_retab = 0
" let g:autoformat_remove_trailing_spaces = 1
let g:autoformat_verbosemode = 1
let g:formatdef_cxx  = '"astyle --style=allman --pad-oper --pad-header --add-braces --unpad-paren"'
let g:formatters_cpp = ['cxx']
let g:formatters_c   = ['cxx']


""""""""""""""""""""""""""""""
" => vim-operator-flashy
""""""""""""""""""""""""""""""
" map y <Plug>(operator-flashy)
" nmap Y <Plug>(operator-flashy)$


""""""""""""""""""""""""""""""
" => vim-sneak,defaut 2 character
""""""""""""""""""""""""""""""
nmap f <Plug>Sneak_s
nmap F <Plug>Sneak_S
xmap f <Plug>Sneak_s
xmap F <Plug>Sneak_S
omap f <Plug>Sneak_s
omap F <Plug>Sneak_S
    
" you can custom <count> character
" nnoremap <silent> f :<C-U>call sneak#wrap('',           3, 0, 2, 1)<CR>
" nnoremap <silent> F :<C-U>call sneak#wrap('',           3, 1, 2, 1)<CR>
" xnoremap <silent> f :<C-U>call sneak#wrap(visualmode(), 3, 0, 2, 1)<CR>
" xnoremap <silent> F :<C-U>call sneak#wrap(visualmode(), 3, 1, 2, 1)<CR>
" onoremap <silent> f :<C-U>call sneak#wrap(v:operator,   3, 0, 2, 1)<CR>
" onoremap <silent> F :<C-U>call sneak#wrap(v:operator,   3, 1, 2, 1)<CR>

""""""""""""""""""""""""""""""
" => vim-vim-expand-region
""""""""""""""""""""""""""""""
map <leader>k <Plug>(expand_region_expand)
map <leader>j <Plug>(expand_region_shrink)


""""""""""""""""""""""""""""""
" => vim-repeat
""""""""""""""""""""""""""""""
silent! call repeat#set("\<Plug>MyWonderfulMap", v:count)


""""""""""""""""""""""""""""""
" => ConqueTerm
""""""""""""""""""""""""""""""
" command! -nargs=+ -complete=shellcmd ConqueTermSplit call conque_term#open(<q-args>, ['belowright split'])
command! -nargs=+ -complete=shellcmd ConqueTermSplit call conque_term#open(<q-args>, ['split'])

""""""""""""""""""""""""""""""
" => cpp-mode
""""""""""""""""""""""""""""""
map <leader>gc :GoToFunImpl<cr>

""""""""""""""""""""""""""""""
" => echodoc.vim
""""""""""""""""""""""""""""""
set cmdheight=2
let g:echodoc_enable_at_startup = 1

""""""""""""""""""""""""""""""
" => LeaderF
""""""""""""""""""""""""""""""
let g:Lf_ShortcutF = '<c-f>'
let g:Lf_ShortcutB = '<c-b>'
let g:Lf_CursorBlink = 0

" map <leader>lf :LeaderfFile<cr>
" map <leader>lb :LeaderfBuffer<cr>
" map            :LeaderfBufferAll<cr>
" map            :LeaderfMru<cr>
" map            :LeaderfMruCwd<cr>
map <leader>lt :LeaderfTag<cr>
" map            :LeaderfBufTag<cr>
" map            :LeaderfBufTagAll<cr>
map <leader>lf :LeaderfFunction<cr>
" map            :LeaderfFunctionAll<cr>
map <leader>ll :LeaderfLine<cr>
" map            :LeaderfLineAll<cr>
" map            :LeaderfHistoryCmd<cr>
map <leader>lh :LeaderfHistorySearch<cr>
" map            :LeaderfSelf<cr>
" map            :LeaderfHelp<cr>
map <leader>lc :LeaderfColorscheme<cr>

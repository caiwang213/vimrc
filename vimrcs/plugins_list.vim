set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" On https://github.com/ usfing: username/plugname 
Plugin 'Valloric/YouCompleteMe'
Plugin 'scrooloose/syntastic'

Plugin 'SirVer/ultisnips'

" On http://vim-scripts.org/vim/scripts.htm using: plugname
Plugin 'a.vim'
"Plugin 'L9'

" All of your Plugins must be added before the following line
call vundle#end()            " required
